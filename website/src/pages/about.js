import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"

function AboutPage() {
  return (
    <Layout>
      <h1>About Us</h1>
      <a
        href="https://rudijs.github.io"
        target="_blank"
        rel="noopener noreferrer"
      >
        rudijs.github.io
      </a>
      <br />
      <Link to="/">Home</Link>
    </Layout>
  )
}

export default AboutPage
